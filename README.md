# phonetic

VueJS-based web app which makes speaking over the phone easier with a read-along phonetic output of your input.

View at: https://neromaki.github.io/phonetic/